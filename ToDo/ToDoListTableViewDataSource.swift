//
//  ToDoListTableViewDataSource.swift
//  ToDo
//
//  Created by Ryan Breaker on 8/20/15.
//  Copyright © 2015 Ryan Breaker. All rights reserved.
//

import UIKit

class ToDoListTableViewDataSource: NSObject, UITableViewDataSource {

	var toDoItems: [ToDoItem] = [ToDoItem(itemName: "Foo"), ToDoItem(itemName: "Bar")]

	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 1
	}

	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return toDoItems.count
	}

	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier("ListCell")!
		let item = toDoItems[indexPath.row]

		cell.textLabel!.text = item.itemName

		if item.completed {
			cell.accessoryType = .Checkmark
		} else {
			cell.accessoryType = .None
		}

		return cell
	}

	// Override to support editing the table view.
	func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
		if editingStyle == .Delete {
			// Delete the row from the data source
			toDoItems.removeAtIndex(indexPath.row)
			tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
		}
	}

	func addToDoItem(newToDoItem: ToDoItem) {
		toDoItems.append(newToDoItem)
	}
}
