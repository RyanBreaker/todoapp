//
//  ToDoItem.swift
//  ToDo
//
//  Created by Ryan Breaker on 8/20/15.
//  Copyright © 2015 Ryan Breaker. All rights reserved.
//

import Foundation

class ToDoItem: Equatable {

	var itemName: String
	var completed: Bool = false

	init(itemName: String = "") {
		self.itemName = itemName
	}
}

func == (lhs: ToDoItem, rhs: ToDoItem) -> Bool {
	return (lhs.itemName == rhs.itemName) && (lhs.completed == rhs.completed)
}
