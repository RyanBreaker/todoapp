//
//  ToDoItemsTableViewController.swift
//  ToDo
//
//  Created by Ryan Breaker on 8/20/15.
//  Copyright © 2015 Ryan Breaker. All rights reserved.
//

import UIKit

class ToDoItemsTableViewController: UITableViewController {

	var dataSource: ToDoListTableViewDataSource { get {
		return tableView.dataSource as! ToDoListTableViewDataSource
	}}

    override func viewDidLoad() {
        super.viewDidLoad()

		self.navigationItem.leftBarButtonItem = self.editButtonItem()
    }

	override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
		let completed = dataSource.toDoItems[indexPath.row].completed
		dataSource.toDoItems[indexPath.row].completed = !completed
		tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
		return nil
	}

	// Placeholder to allow unwind from Cancel button
	@IBAction func unwindAction(unwindSegue: UIStoryboardSegue) {}

	// Unwind method for Save button
	@IBAction func unwindAndSaveAction(unwindSegue: UIStoryboardSegue) {
		// `as!` is allowed because we know the Save button was used to trigger this as it's
		// the only object connected to this method
		let addToDoController = unwindSegue.sourceViewController as! AddToDoItemViewController
		// Check for valid input, nil if invalid
		if let tdi = addToDoController.toDoItem {
			dataSource.toDoItems.append(tdi)
			tableView.reloadData()
		}
	}
}
