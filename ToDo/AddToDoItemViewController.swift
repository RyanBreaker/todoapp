//
//  AddToDoItemViewController.swift
//  ToDo
//
//  Created by Ryan Breaker on 8/20/15.
//  Copyright © 2015 Ryan Breaker. All rights reserved.
//

import UIKit

class AddToDoItemViewController: UIViewController, UITextFieldDelegate {

	@IBOutlet weak var saveButton: UIBarButtonItem!
	@IBOutlet weak var textField: UITextField!

	var toDoItem: ToDoItem? { get {
		// Safe conversion attempt
		if let s = textField.text {
			// Final check for empty string
			return (s.isEmpty ? nil : ToDoItem(itemName: s))
		} else {
			// Return nil if failed
			return nil
		}
	}}

	// MARK: - UITextFieldDelegate methods

	func textFieldShouldReturn(textField: UITextField) -> Bool {
		textField.resignFirstResponder()
		return false
	}
}
