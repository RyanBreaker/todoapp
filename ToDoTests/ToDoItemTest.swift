//
//  ToDoItemTest.swift
//  ToDo
//
//  Created by Ryan Breaker on 8/21/15.
//  Copyright © 2015 Ryan Breaker. All rights reserved.
//

import XCTest
@testable import ToDo

class ToDoItemTest: XCTestCase {

	func testToDoItemEquality() {
		let s = "Foo"
		let lhs = ToDoItem()
		let rhs = ToDoItem(itemName: s)

		XCTAssertNotEqual(lhs, rhs)

		lhs.itemName = s
		XCTAssertEqual(lhs, rhs)

		XCTAssertFalse(lhs.completed)
		XCTAssertFalse(rhs.completed)

		lhs.completed = true
		XCTAssertNotEqual(lhs, rhs)

		rhs.completed = true
		XCTAssertEqual(lhs, rhs)

		XCTAssertTrue(lhs.completed)
		XCTAssertTrue(lhs.completed)
	}

}
